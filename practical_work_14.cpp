﻿#include <iostream>

using namespace std;

class Vector
{
public:
	Vector() : x(0), y(0), z(0)
	{}
	Vector(double _x, double _y, double _z) : x(_x), y(_y), z(_z)
	{}
	void Show()
	{
		cout << "The length of your vector: " << sqrt(pow(x, 2) + pow(y, 2) + pow(z, 2)) << endl;
	}
private:
	double x, y, z;
};

int main()
{
	double x, y, z;
	cout << "Enter x: ";
	cin >> x;
	cout << "Enter y: ";
	cin >> y;
	cout << "Enter z: ";
	cin >> z;
	Vector v(x, y, z);
	v.Show();
}
